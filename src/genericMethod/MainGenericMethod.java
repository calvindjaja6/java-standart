package genericMethod;

import genericClass.MyData;

public class MainGenericMethod {
    public static void main(String[] args) {
        String[] names = {"Eko","Kurniawan","Khannedy"};
        Integer[] values = {1, 2, 3, 4, 5};

        System.out.println(ArrayHelper.<String>count(names));
        System.out.println(ArrayHelper.count(values));
    }
}
