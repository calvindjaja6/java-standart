package genericClass;

public class MainCovariantClass {
    public static void main(String[] args) {
        MyData<String> data = new MyData<>("Eko");
        process(data);
    }

    public static void process(MyData<? extends Object> data){
        Object object = data.getData();
//        data.setData("Eko"); //Error
    }
}
