package genericClass;

public class MainWildcardClass {
    public static void main(String[] args) {
        printLength(new MyData<>(100));
        printLength(new MyData<>("Eko"));
        printLength(new MyData<>(true));
    }

    public static void printLength(MyData<?> data){
        System.out.println(data.getData());
    }
}
