package genericClass;

public class MainContravariantClass {
    public static void main(String[] args) {
        MyData<Object> objectMyData = new MyData<>("Eko");
        MyData<? super String> myData = objectMyData;
    }

    public static void process(MyData<? super String> myData){
        myData.setData("Eko");
    }
}
